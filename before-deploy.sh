#!/bin/sh -e
if [ "x$CI_COMMIT_REF_NAME" = "xmaster" -o "x$FORCE_DEPLOY" = "xtrue" ]; then
    echo -e "$PGP_KEY" > cd/codesigning.asc
    gpg --batch --fast-import cd/codesigning.asc
    gpg --list-keys
else
    echo "Skipping deployment because branch $CI_COMMIT_REF_NAME is not master"
fi

