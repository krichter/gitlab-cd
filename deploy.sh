#!/bin/sh -e
if [ "x$CI_COMMIT_REF_NAME" = "xmaster" -o "x$FORCE_DEPLOY" = "xtrue" ]; then
    mvn --batch-mode deploy -P ossrh --settings cd/mvnsettings.xml -Dmaven.test.skip=true
else
    echo "Skipping deployment because branch $CI_COMMIT_REF_NAME is not master"
fi
